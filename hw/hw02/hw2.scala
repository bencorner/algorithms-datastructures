//   bcorner
//   Ben Corner
//   MATH382 -- HW #2



object hw2 {

    //
    // itMergesort
    //
    // 
    // 
      
    def itMergesort(a:Array[Int]):Unit = {
	    
	val n:Int = a.length;
	var i:Int = 1;
	var j:Int = 0;
	    
	// "i" is the size of the subsections we're merging 			     
	// Our variable "j" is the index of the first element			     
	//    of the current (to-be-merged) array subsection of size "i"	     
	//    We increment by 2*i because each merge takes 2 subsections of size "i".
	// merge subsection a[j:j+i-1] with a[j+i:j+2*i-1]                           
	    

	while (i < n) {	                        
	    for (j <- 0 until (n-i) by (2*i) ) {  
			                                
			                                 
		merge(a, j, j+i-1, j+2*i-1);     
	    }	  
		  
	    i = i*2;		  
	}
	    
    }





    //
    // merge
    //
    // This procedure merges two sorted portions of an array, the
    // elements in range p..q, and the elements in range q+1..r.  The
    // resulting sorted values are put in the range p..r.  
    //
    // It copies the two portions into arrays left and right in order
    // to merge those values back into the combined portion in a.
    // 
    def merge(a:Array[Int], p:Int, q:Int, r:Int) {
    
	// make a copy of the left half, with an
	// aditional "sentinel" value of infinity.
	// (see CLRS page 31).
	//
	val ln:Int = q - p + 1;
	val left:Array[Int] = new Array[Int](ln+1);
    
	for (i <- 0 until ln) {
	    left(i) = a(p+i);
	}
	left(ln) = Int.MaxValue;
    
	// make a copy of the right half
	val rn:Int = r - q;
	val right:Array[Int] = new Array[Int](rn+1);
    
	for (i <- 0 until rn) {
	    right(i) = a(q+1+i);
	}
	right(rn) = Int.MaxValue;
    
	var li:Int = 0;
	var ri:Int = 0;
	for (i <- p to r) {
	    if (left(li) <= right(ri)) {
		a(i) = left(li);
		li = li + 1;
	    } else {
		a(i) = right(ri);
		ri = ri + 1;
	    }
	}
    }



  
    //
    // main
    //
    // Tests insertion sort and merge sort.
    //
    def main(args:Array[String]):Unit = {

	val ints:Array[Int] = new Array[Int](args.length);

	for (i <- 0 until args.length) {
	    ints(i) = args(i).toInt;
	}

	print("BEFORE:")
	for (v <- ints) {
	    print(v + " ");
	}
	println();

	// Test itMergesort
	itMergesort(ints);
      
	println("AFTER ITMERGESORT:")
	for (v <- ints) {
	    print(v + " ");
	}
	println();

    }
}
      

