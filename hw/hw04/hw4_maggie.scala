class Term(c:Int,p:Int) {
  var coeff:Int = c;
  var power:Int = p;
  var next:Term = null; 
  def this(t:Term) { this(t.coeff,t.power); }
  def output():Unit = { print(this.toString); }
  override def toString():String = { 
    val c:String = if (coeff == -1 && power != 0) "-" else if (coeff == 1 && power != 0) "" else coeff.toString;
    val x:String = { if (power > 0) "x" else "" } + { if (power > 1) { "^" + power} else "" }; 
    return c + x;
  }
}

class Poly(c:Int,p:Int) {
  var firstTerm:Term = new Term(c,p);
  var lastTerm:Term = firstTerm;
  def this(t:Term) = { this(t.coeff,t.power); }
  def apply(value:Int):Int = { return evalAt(value); }  
  def evalAt(value:Int):Int = { return 0; } // ***
  def +(q:Poly):Poly = {

  	var t = p.first
  	var s = q.first
	var v:Term = new Term(0,0)
  	var r = new Poly(0,0)
  	(v,t,s) = merge(t,s)
  	r.first = v
  	r.last = v
  	while (t != null and s != null) {
  		var lastadded:Term = v
  		(v,t,s) = merge (t,s)
  		lastadded.next = v
  									}
	r.last = v
	return r
  						} // ***
  
  
  def *(q:Poly):Poly = { return new Poly(0,0); } // ***
  def atZero:Int = {
  	if last.power == 0 {
  		return last.coeff
  						}
  	else {
  		return 0
  			}
  					} // *

  def degree:Int = {
	if first.coeff == 0  {
		return 0
						}
	else {
		return first.power
		  }				
	  					} // *

  def output():Unit = { print(this); }
  override def toString():String = { return firstTerm.toString; } // ***
  
  
  def merge(t:Term,s:Term):(Term,Term,Term) = {
  	if (s == null || t.power > s.power) {
  		var v:Term = new Term(t.coeff,t.power)
  		return(v,t.next,s)
  										}								
  	else if (t == null || t.power < s.power) {
		var v:Term = new Term(s.coeff,s.power)
  		return (v,t,s.next)
  											}
  	else {
  		var v:Term = new Term(s.coeff+t.coeff,t.power)
  		return(v,t.next,s.next)
  		  }
  												}

  
  
}





object Poly {
  def apply(c:Int,p:Int):Poly = new Poly(c,p);
}

object hw4 extends App {
  override def main(args:Array[String]):Unit = {
    val p = Poly(10,2)+Poly(2,1)+Poly(5,0);
    val q = Poly(1,2)+Poly(-2,6)+Poly(1,10);
    println("The polynomial "+p+" times the polynomial "+q);
    println("is the polynomial "+(p*q)+".");
    println("The polynomial "+p+" evaluated at x = 2 is "+p(2)+".");
  }
}
