class Term(c:Int,p:Int) {
    val coeff:Int = c;
    val power:Int = p;
    var next:Term = null; 
    def this(t:Term) { this(t.coeff,t.power); }
    def output():Unit = { print(this.toString); }
	override def toString():String = { 
	val c:String = if (coeff == -1 && power != 0) "-" else if (coeff == 1 && power != 0) "" else coeff.toString;
	val x:String = { if (power > 0) "x" else "" } + { if (power > 1) { "^" + power} else "" }; 
	return c + x;
    }
}

class Poly(c:Int,p:Int) {
    var firstTerm:Term = new Term(c,p);
    var lastTerm:Term = firstTerm;
    def this(t:Term) = { this(t.coeff,t.power); }
	def apply(value:Int):Int = { return evalAt(value); }  
	def evalAt(value:Int):Int = { return 0; } // ***
	
	
	def +(q:Poly):Poly = 
	{ 
	    var polya:Poly = new Poly(this);  // get it? Polya! haha. ha.
	    var polyb:Poly = new Poly(q);
	    
	    while ()
	    
	    // find highest powered coeff not yet included in result
	    
	    
	    
	    

	    
	    // merge term (or summation of both term's coeffs if equally high power)
	    
	    

	    
	    return new Poly(0,0); 
	    
	} // ***
	
	def *(q:Poly):Poly = { return new Poly(0,0); } // ***
	def atZero:Int = { return 0; } // ***
	def degree:Int = { return 0; } // ***
	def output():Unit = { print(this); }
	override def toString():String = { return firstTerm.toString; } // ***
}



object Poly {
    def apply(c:Int,p:Int):Poly = new Poly(c,p);
}

object hw4 extends App {
    override def main(args:Array[String]):Unit = {
	val p = Poly(10,2)+Poly(2,1)+Poly(5,0);
	val q = Poly(1,2)+Poly(-2,6)+Poly(1,10);
	println("The polynomial "+p+" times the polynomial "+q);
	println("is the polynomial "+(p*q)+".");
	println("The polynomial "+p+" evaluated at x = 2 is "+p(2)+".");
    }
}
