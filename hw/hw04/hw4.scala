class Term(c:Int,p:Int) {
    val coeff:Int = c;
    val power:Int = p;
    var next:Term = null; 
    def this(t:Term) { this(t.coeff,t.power); }
    def output():Unit = { print(this.toString); }
    	override def toString():String = { 
	val c:String = if (coeff == -1 && power != 0) "-" else if (coeff == 1 && power != 0) "" else coeff.toString;
	val x:String = { if (power > 0) "x" else "" } + { if (power > 1) { "^" + power} else "" };  
	return c + x;
    }
}

class Poly(c:Int,p:Int) {
    var firstTerm:Term = new Term(c,p);
    var lastTerm:Term = firstTerm;
      
    def this(t:Term) = { 
	this(t.coeff,t.power); 
    }
	
	def apply(value:Int):Int = { return evalAt(value); }  

	// EVALAT
	//
	// This function evaluates the polynomial at 'value'.
	// The implementation used here takes advantage of Horner's rule.
	// In order to account for polynomialsof degree k with fewer than k terms,
	// we calculate the difference between each swuccessive terms' powers.
	//     * If diff > 1, multiply by 'value' for every power in between k, k.next.
	//     * Else, multiply result by value and add k's coefficient.
	//
	// BUGS: (fixed?) This code doesn't work for polynomials whose smallest term's power
	//  is greater than 1.    <----** Oh, wait, I think I fixed this. 
	//     
	// Returns an Int.    
	
	def evalAt(value:Int):Int = 
	{  
	    var k = this.firstTerm;
	    var result = 0;
	    var diff:Int = 0;
	    var j:Int = 0;

	    while (k != null) {  

		// Fix for polynomials whose smallest term has power greater than 1.
		// I think.
		if (k.next == null) {
		    result = result*value + k.coeff;
		    
		    for (j <- 1 to k.power)
			result = result*value;
		    return (result);
		}

		    
		// Account for missing multiplications of 'value' resulting from
		// a polynomial with degree 'k' and fewer than 'k' terms (missing some powers)
		diff =  k.power - k.next.power;
		if (diff > 1) {                  
		    for (j <- 1 to diff) {
			result = result*value;
		    }
		}
				
		result = result*value + k.coeff;  // Horner's Rule.
		k = k.next;
	    }
	    return result;
	} // ***
	

	// PLUS
	//	
	// Assumes both Polys are individually well-formed:
	// 	   * Powers in decreasing order
	//	   * At most one term with power 'k' for each k.
	//	   * No zero-coefficients
	//
	// Traverses both polynomials, only going to next term in one when
	// it has the highest degree not-yet-included in our result.

	def +(q:Poly):Poly =
	{   
	    var termP:Term = firstTerm;
	    var termQ:Term = q.firstTerm;
	    // Result of sum to be stored in Poly 'sumResult'

	    
	    // This is that sketchy shit everyone's been messing with...
	    // work dat workaround, baby!
	    // create NEW poly , so we're not referencing the old polys
	    var ghetto = merge(firstTerm, q.firstTerm);
	    var sumResult:Poly = new Poly(ghetto._1); 
	    termP = ghetto._2;
	    termQ = ghetto._3;
	    
	    while (termP != null & termQ != null) 
		{
		    ghetto = merge(termP, termQ);
		    sumResult.lastTerm.next = ghetto._1; termP = ghetto._2; termQ = ghetto._3;
		    sumResult.lastTerm = sumResult.lastTerm.next;
		}

	    //once either termP or termQ is null:

	    // stitching the remaining terms of polynomial 'p' onto sumResult
	    while (termP != null) {    
		sumResult.lastTerm.next = termP;
		sumResult.lastTerm = sumResult.lastTerm.next;
		termP = termP.next;
	    }

	    // stitching the remaining terms of polynomial 'q' onto sumResult
	    while (termQ != null) {
		sumResult.lastTerm.next = new Term(termQ);
		sumResult.lastTerm = sumResult.lastTerm.next;
		termQ = termQ.next;
	    }


	    return sumResult;    
	}



	
	// MERGE
	//
	// Takes two terms (P, Q) and figures out whether the highest-degree
	// term of their sum is
	//     * precisely P or precisely Q
	//    or
	//     * whether P and Q are terms of the same degree,
	//     hence their sum's coeff is the sum of their coeffs.
	//
	// Returns the summation as a Term, and 'advances' the term-traversers. 
	// 
	def merge(termP:Term, termQ:Term):(Term, Term, Term) =
	{
	
	    // advances P to next term when it was the higher-degree term
	    if (termQ == null || termP.power > termQ.power)
		{
		    val termResult:Term = new Term(termP);
		    return (termResult, termP.next, termQ)} 

	    // advances Q to next term when it was the higher-degree term
	    else if (termP == null || termP.power < termQ.power)
		{
		    val termResult:Term = new Term(termQ);
		    return (termResult, termP, termQ.next)}  
	    
	    // advances both P and Q when they were a term of same-degree
	    else 
		{

		    val termResult:Term = new Term( (termP.coeff + termQ.coeff), termP.power);
		    return (termResult, termP.next, termQ.next)  
		    
		    ////// BELOW: waas trying to handle when two terms of same power
		    //////   but opposite-signed coeffs are added (trying to prevent
		    //////   production of terms with 0-coefficients).
		    //////
		    //////   For ex.    MERGE( (2)x^2 ,  (-2)x^2 )
		    //////   should result in no Term being returned. Although, perhaps
		    //////   this problem should be handled in SUM, not MERGE. However,
		    //////   I felt that a term with coeff = 0 should NEVER be produced,
		    //////   not just that such terms should be swept under the rug. 



		    /*
		    if (termP.coeff + termQ.coeff != 0) { //Don't make new term with 0-coeff.
			val termResult:Term = new Term( (termP.coeff + termQ.coeff), termP.power);
		    }
		    
		    else if (termP == null & termQ == null)
		        val termResult:Term = new Term(termP);
			//return (null, null, null);

		    else
			{
			    val sheeeit = merge(termP.next, termQ.next); //hacky...
			    val termResult:Term = new Term(sheeeit._1);
			    termP.next = sheeeit._2;
			    termQ.next = sheeeit._3;
			}  
		    */
		    

		}
	
	}
	
	
	def *(q:Poly):Poly = { return new Poly(0,0); } // ***
	
	

	// ATZERO
	//
	// A polynomial will only have a non-zero value when
	// evaluated at 0 if:
	//     * there is a non-zero coeff for the 0-power term.
	//
	// Otherwise, it will evaluate to 0.
	def atZero:Int = 
	{ 
	    if (lastTerm.power == 0)
	    	return lastTerm.coeff;
	    
	    return 0; 
	} // ***
	

	
	// DEGREE
	// 
	// Returns the highest degree found in this polynomial.
	// Since we decreed that:
	//     * no poly shall have terms with a zero-coefficient
	//     * a well-formed poly must have its terms in descending
	//       order according to term.power,
	// 
	// the term of highest degree is the first term.
	// 
	// If there is no term, I make it return that
	// the polynomial is the zero polynomial.
	def degree:Int = 
	{ 
	    if (firstTerm == null) return 0;
	    return firstTerm.power;
	} // ***
	
	
	
	// This function causes our toString method to be called when we print a Poly.
	def output():Unit = { print(this); }
	



	override def toString():String = { 

	var current:Term  = firstTerm;
	var polyString:String = new String;
	
	while (current != null)
	    {
		polyString = polyString + current.toString;
		current = current.next;
		
		// This 'if' adds a "+" between terms if then next term (exists and) is positive.
		//  (addition: also "+" if coeff. is zero (see MERGE-function BUGS for why zero-coeff
		//    polynomials can exist in my current implementation)
		// Negative terms come with a "-" courtesy of Jim's Term.toString.
		if (current != null) if (current.coeff >= 0) {
			polyString = polyString + "+";}
	    }
	
	return polyString;
    }
}	    



object Poly {
	    def apply(c:Int,p:Int):Poly = new Poly(c,p);
}





object hw4 extends App {
    override def main(args:Array[String]):Unit = {


	// TESTS
	//    Test pairs of polynomials (pi,qi)   <-- sidenote, both "pi" and "qi"are legal 2-letter words in scrabble!

	println("two polynomials");
	val p1 = Poly(2,6)+Poly(10,2)+Poly(2,1)+Poly(5,0);
	val q1 = Poly(3,5)+Poly(-3,6)+Poly(1,10)+Poly(2,2); 
	testPQ(p1,q1);

	
	println("causes a term with a zero-coeff. to be created upon \n addition of two k-th power terms with equal coeffs opposite signs: \n\t (a*x^k) + (-a*x^k) = 0*x^k");
	val p2 = Poly(2,6)+Poly(10,2)+Poly(2,1)+Poly(5,0);
	val q2 = Poly(3,5)+Poly(-2,6)+Poly(1,10)+Poly(2,2); 
	testPQ(p2, q2);	    


	
	println("two constant polynomials");
	val p3 = Poly(5,0);
	val q3 = Poly(3,0);
	testPQ(p3, q3);


	
	println("Here's a bug/unsanitary polynomial: adding a poly with a negative power causes \n bad problems with my horner's rule implementation. And, just, bad in general.");
	val p4 = Poly(4,1)+Poly(7,0)+Poly(5,-1);
	val q4 = Poly(3,0);
	testPQ(p4, q4);



	
	println("High-degree Poly plus a constant Poly");
	val p5 = Poly(4,7);
	val q5 = Poly(3,0);
	testPQ(p5, q5);



	println("High-degree Poly plus another high-degree Poly");
	val p6 = Poly(4,7);
	val q6 = Poly(8,9);
	testPQ(p6, q6);
	

    }


    // Simple test function which prints a series of tests for the passed Polys.
    def testPQ(p:Poly, q:Poly) {
	
	println("\nTESTING:     p(x) = "+p+"    and     q(x) = "+q);
	println("\t     p(2) is "+p(2));
	println("\t     q(5) is "+q(5));
	
	println("\n\tADDING: p + q yields:  "+(p+q)+"  .");
	println("\t\t     (p+q)(2) is "+(p+q)(2));

	println("\n\tDEGREE:  \n\t degree(p) = " + p.degree + "\n\t degree(q) = " + q.degree)

	println("\n\tATZERO:  \n\t atZero(p) = " + p.atZero + "\n\t atZero(q) = " + q.atZero)
	
	println("\n Were p or q modified? \n");
	println("CHECKING:    p(x) = "+p+"    and     q(x) = "+q);
	println("\n********************\n\n\n\n");

    }
}
