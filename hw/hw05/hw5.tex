\documentclass[11pt]{report}
\usepackage{newalg}
\usepackage{latexsym}

\usepackage{graphics}
\usepackage{mathtools}

\usepackage{algpseudocode}



\setlength{\topmargin}{-1in}
\addtolength{\textheight}{2in}
\setlength{\oddsidemargin}{-0.25in}
\setlength{\evensidemargin}{0in}
\setlength{\textwidth}{6.75in}



\newcommand{\bookproblem}[1] {\mbox{\large[\textit{#1}\large]} }
\newcommand{\T}[1] {\operatorname{T}(#1)}




\begin{document}

\pagestyle{empty}

\begin{center}
{\Large Math 382: Homework 5}
{\large Heaps and Trees}

{\bf Scala code Due:} Friday, March 1, 2012.\vspace{0.5in}
\end{center}

\begin{enumerate}
\item Using ternary heaps, we must make some modifications to our heap
  array index calculations:
  
  \begin{itemize}
  \item[] $\mbox{\sc Parent}\,(i)\ : \ \lfloor \frac{i+1}{3} \rfloor $
  \item[] $\mbox{\sc Left}\,(i)\ : \ 3 i - 1 $
  \item[] $\mbox{\sc Middle}\,(i)\ : \ 3 i $
  \item[] $\mbox{\sc Right}\,(i)\ : \ 3 i + 1 $
  \end{itemize}

  An {\sc Insert}($k$) method would work by placing the value $k$ in
  the next available slot in the array (assuming the current ternary
  heap is occupying a single contiguous block of memory), and then the
  node containing $k$ would continue to swap its value with its
  parent's (and then parent's parent's) value until its new parent's
  value is greater than $k$, or until $k$ has become the root node.

  A {\sc Remove-Max} method is fairly straightforward. First, store
  the root node's value in $heapMax $ so we are able to return the
  heap's max. Then, move the largest of the root node's children up to
  become the new root node. Then, until we arrive at a leaf node,
  recursively repeat the same process, pulling up the largest child
  node to fill the slot of its recently pulled-upward parent node.


  To find an exact expression for the depth, $d$, of a ternary heap as
  a function of the total number of nodes, $n$, we note:
$$  1 + 3 + 3^2 + \ldots + 3^{d-1} \  \leq \  n \ \leq \  1 + 3 + 3^2 + \ldots + 3^{d} $$
Rewrite as a geometric sum:
\[ \sum_{i=0}^{d-1} 3^i \ \leq \ n \ \leq \ \sum_{i=0}^{d}{3^i} \quad
\implies \quad \frac{3^d - 1}{2} \ \leq \ n \ \leq \ \frac{3^{d+1} -
  1}{2} \] We can further rewrite the inequaity:
\[ 3^n \ \leq \ 2n + 1 \ \leq \ 3^{d+1} \quad \implies \quad d \ \leq
\ \log_{3}{(2n +1)} \ \leq \ d+1 \] From this, we find that the depth
of an $n$-node ternary heap is given by: \fbox{ $\displaystyle \lceil{
    \log_{3}(2n+1)}\rceil $}




\item \bookproblem{CLRS 6.3-a} A Young Tableau containing the elements
  $ \{ 9, 16,3, 2, 4, 8, 5, 14, 12 \} $:
    
  % \vspace{0.2in}
  \begin{center}
    \begin{tabular}{ |c|c|c|c| }
      \hline
      $2$ & $3$ & $4$ & $5$ \\ \hline
      $8$ & $9$ & $12$ & $\infty$ \\ \hline
      $14$ & $\infty$ & $\infty$ & $\infty$ \\ \hline
      $\infty$ & $\infty$ & $\infty$ & $\infty$ \\ 
      \hline
    \end{tabular}
  \end{center}
    

  \newpage


  \bookproblem{CLRS 6.3-c} A pseudocode algorithm to implement
  {\textsc Extract-Min} on a non-empty $m \times n$ Young Tableau that
  runs in $\operatorname{O}\bigl(m + n\bigr) $ time:

  \hfill\begin{minipage}{\dimexpr\textwidth-3cm}
    \begin{algorithm}
      {Extract-Min}{A_{m x n}, {[ (i = 0, j = 0)]}}
      right \= (i, j+1) \\
      down \= (i+1, j) \\
      \begin{IF}{(i,j) = (0,0)}
        min \= A(i,j)
        \ELSE \\
        {min \= 0 }
      \end{IF} \\
      \vspace{1cm}
      \begin{IF}{(right.cols < n) \AND A(right) \leq A(down)}
        A(i,j) \= A(right) \\
        \RETURN (min + \CALL{Extract-Min}(A, right) )
        \ELSEIF{(down.rows < m) \AND A(down) \leq A(right)}
        A(i,j) \= A(down) \\
        \RETURN (min + \CALL{Extract-Min}(A, down) )
        \ELSE \\
        \RETURN 0
      \end{IF}
    \end{algorithm}
  \end{minipage}
  
  The smallest value in a Young Tableau is found in the top left
  corner. This value is stored as $min$. Compare the current value in
  the top left slot with its $down$ and $right$ neighbours. The
  smallest of these values is swapped with the current top left
  value. If this swap occurs $right$, we recursively repeat this
  swapping procedure on the sub-matrix obtained by eliminating the
  left-most column. Similarly, a swap occurring $down$ results in
  running the swapping-procedure on the sub-matrix obtained by
  removing the top row. This swapping occurs until the top left
  element of the sub-matrix is smaller than its $right$ and $down$
  neighbours--or until one of the sub-matrix dimensions is 0.


  We now find a recurrence in order to calculate $\T{p}$, a worst-case
  runtime of {\sc Extract-Min} for an \mbox{$m \times n$} Young
  Tableau. We note that since the worst-case scenario necessarily
  terminates at the bottom-right slot of the tableau, a path involving
  \mbox{$(m-1) + (n-1)$} swaps,
  \begin{align*}
    \begin{aligned}
      \T{m+n} = \T{p} &= \T{p-1} + 1 \\
    \end{aligned}
%
    \begin{aligned}
      \hspace{3cm}
    \end{aligned}
%
    \begin{aligned}
      && \T{2} &= 0 \\
    \end{aligned}
%
  \end{align*}

  We see that generally:
  \[\T{p} \ = \ \T{p-(p-2)} + \sum_{i=1}^{p-2}{1} \ = \ \T{2} + p-2 \
  = \ p-2 \qquad \implies \qquad \T{m+n} = m+n-2 \newline
  \]
  \begin{center}Thus, \textsc{Extract-Min}$(A_{m \times n}) \in
    \operatorname{O}\bigl(m + n \bigr)$\end{center}

  \bigskip \bookproblem{CLRS 6.3-d} This question touches on a small
  error in my pseudocode from part $c$ -- namely, I don't explicitly
  write an $\infty$ to fill the missing slot created by removing the
  minimum value. The key realization to have when figuring out how to
  insert an element in a non-full $m \times n$ tableau is that the
  bottom-right slot of a min-tableau is the max of a max-tableau whose
  values are in decreasing order from bottom up and from right to the
  left. So, since the tableau is non-full, there is at least one
  $\infty$ term in the tableau, which implies that there must be an
  $\infty$ (or, empty slot) at the bottom-right. Insert the new value
  at the bottom right. Then, practically the same code as part $c$
  would restore the tableau-ness, simply switching $right$ and $down$
  to $left$ and $up$, and flipping the inequalities so that you are
  swapping in the maximum neighbouring value -- and of course changing
  the requisite column- and row-bound checks.

  \newpage
\item The following pseudocode cleans up the node-linking after
  insertion of an element into a binary search tree where each node
  $x$ contains an $x.next$ and $x.prev$ field, linked according to the
  tree-ordering.
  

  \hfill\begin{minipage}{\dimexpr\textwidth-3cm}
    \begin{algorithm}
      {Post-Insertion}{k}
      \begin{IF}{k > k.parent}
        k.next \= (k.parent).next; \quad k.prev \= (k.parent); \\
        (k.prev).next \= k; \quad (k.next).prev \= k;
        \ELSE \\
        k.next \= k.parent; \quad  k.prev \= (k.parent).prev \\
        (k.next).prev \= k; \quad (k.prev).next \= k
      \end{IF}
    \end{algorithm}
  \end{minipage}
  

\item As mentioned by a certain little birdie, while it may be
  tempting to track the overall sequence position for each and every
  node in an implementation of such a linked-binary-search-tree
  implementation, one can actually have a much more efficient
  framework if each node instead simply tracks the height of its left
  subtree. To be specific,

  
  \hfill\begin{minipage}{\dimexpr\textwidth-3cm}
    \begin{algorithm}
      {S.Access}{n, x} \IF{n = x.left.size +1} \qquad \RETURN{
        x.value} \ELSEIF{n < x.left.size} \RETURN \CALL{Access}(i,
      x.left) \ELSEIF{n > x.left.size +1} \RETURN \CALL{Access}(n,
      x.right)
      \endIF
    \end{algorithm}

    \begin{algorithm}
      {S.Insert}{n,x}
      % \mbox{int $i$} \= \mbox{root}.index \\
      % \REPEAT{}
      % \IF{ i.left.size = n}
      % i.left.size ++ \\
      % \CALL{S.Post-Insert} \quad \COMMENT{basically, add in $x$ by running \mbox{\sc Post-Insert}} \\
      % \ELSEIF{ i.left.size > n} i \= i.left
      % \endIF
      % % \ELSEIF{}
      % \UNTIL{ i > S.length }
    \end{algorithm}

    
    % \begin{algorithm}
    %   {S.Insert}{n,x}
    %   \mbox{int $i$} \= \mbox{root}.index \\
    %   \REPEAT{} \IF{ i.left.size = n}
    %   i.left.size ++ \\
    %   \CALL{S.Post-Insert} \quad \COMMENT{basically, add in $x$ by running \mbox{\sc Post-Insert}} \\
    %   \ELSEIF{ i.left.size > n} i \= i.left
    %   \endIF
    %   % \ELSEIF{}
    %   \UNTIL{ i > S.length }
    % \end{algorithm}
  \end{minipage}
  

\end{enumerate}
\end{document}

