//
// MATH 382 
//
// HW6 program: Displaying the Splay 
// 
// The code below implements a Sequence data structure as a
// binary tree, as prescribed by Problem #4 of Homework 5.
// It supports three operations, described in pseudo-code as
//
//   v := s.ACCESS-AT(i): get the i-th value in Sequence s
//   s.UPDATE-AT(i,v'): change the i-th value to v'
//   s.INSERT-AT(i,v): insert a value v at position i 
//
// These are implemented in the four methods
//
//   apply
//   update
//   +=
//
// in the Scala code below.
//
// Recall that a Sequence provides a representation of
// a resizable array (in some languages, like C++ and Java,
// this is called a "vector" data structure).  So, in essence
// you can think of a Sequence[Int] like
//
//    3,1,8,5
//
// as an array
//
//     0   1   2   3
//   +===+===+===+===+
//   |   |   |   |   |
//   | 3 | 1 | 8 | 5 |
//   |   |   |   |   |
//   +===+===+===+===+
//
// but an operation like "insert 7 at 2" leads to a larger
// structure
//
//     0   1   2   3   4
//   +===+===+===+===+===+
//   |   |   |   |   |   |
//   | 3 | 1 | 7 | 8 | 5 |
//   |   |   |   |   |   |
//   +===+===+===+===+===+
//
// corresponding to the Sequence
//
//    3,1,7,8,5
//
// Recall also that our solution to this is that for a 
// Sequence S that mimicks this array
//
//     0   1         i         n-1 
//   +===+===+=...=+===+===...+===+
//   |   |   |     |   |      |   |
//   |   |   |     | v |      |   |
//   |   |   |     |   |      |   |
//   +===+===+=...=+===+===...+===+
//   
// be represented as binary a tree with a root node containing
// the value v, and whose left subtree has i values stored in 
// it (the root's "left size" is i).
//
//                 v
//                / \
//       S[0..i-1]   S[i+1..n-1]      
//
// This kind of implementation relies on a special SeqNode
// object, one that represents the node carrying v.  Its 
// implementation consists of
//
class SeqNode[T](v:T) {

  var leftSize:Int = 0;         // the size of this node's left subtree

  var value:T = v;              // the value stored at this node

  var left:SeqNode[T] = null;   // the left and right subtrees
  var right:SeqNode[T] = null;

  var column:Int = 0;           // can be used for display
  var row:Int = 0;
}

//
// You can create any such node in Scala with an expression like
//
//      new Sequence[Int](7)
//
// In the above, we're creating a node for a Sequence[Int] containing
// the value 7.
//
// These SeqNode objects form the basis of our binary tree implementation
// of a Sequence[T] collection class, shown below.
//
// (Following the "class Sequence[T] {...}" code is a description of your
//  programming assignment)
//
 
class Sequence[T] {


    // ******** INSTANCE VARIABLES ********

    //
    // Each Sequence[T] tree just knows its root node along with the
    // number of items stored in the Sequence (for index range checks).
    // 
    var root:SeqNode[T] = null;
    var size:Int = 0;

    // ******** API SUPPORT (ERRORS) ********

    // IndexOutOfRangeException
    //
    // This exception is raised when code attempts to access a value
    // at an index outside of the range 0..size-1
    class IndexOutOfRangeException extends Exception;

    class NullRightSubtreeException extends Exception;

    class NullLeftSubtreeException extends Exception;
    
    // ******** API METHODS ********

    // access/apply
    //
    // v := S.ACCESS-AT(i): get the value at position i
    //
    // Scala use:  
    //
    //         val S:Sequence[..] = ...
    //
    //         ...S(i)...
    //
    // This relies of the helper function "lookBelowFor", which gets
    // the node at position i in the Sequence.
    //
    def apply(i:Int):T = {

	if (0 <= i && i < this.size) {

	    val x:SeqNode[T] = lookBelowFor(this.root,i);
	    this.root = x;      // accessed (and thus splayed) i^th node is the new root of the tree.
	    return x.value;

	} else {

	    throw new IndexOutOfRangeException;

	}
    }

    //
    // insert/+=
    //
    // S.INSERT-AT(i,v): insert a value v into position i
    //
    // Scala use:  
    //
    //         val S:Sequence[..] = ...
    //         ...
    //         S += (i,v);
    //
    // This relies of the helper function "insertBelowAt", which 
    // adds a new node with v, one that corresponds to position i.
    //
    def +=(i:Int,w:T):Unit = {

	if (0 <= i && i <= this.size) {

	    this.root = insertBelowAt(this.root,i,w);
	    this.size = this.size+1;
	    // this(i); // performs the access (leading to a SPLAY)
	    this.root = lookBelowFor(this.root, i); // performs the access (leading to a SPLAY). Sets new root to splayed node.
	    
	} else {

	    throw new IndexOutOfRangeException;

	}

    }
    
    //
    // append/+=
    //
    // S.APPEND(v): puts a value v at the end of S, a new last position
    //
    // Scala use:  
    //
    //         val S:Sequence[..] = ...
    //         ...
    //         S += v;
    //
    // This is just the same as 
    //
    //         S += (S.size,v);
    //
    //
    def +=(w:T):Unit = {

	this += (this.size,w);

    }
    
    //
    // update
    //
    // S.UPDATE-AT(i,w): replaces the value at position i with w.
    //
    // Scala use:
    //
    //         val S:Sequence[..] = ...
    //         ...
    //         S(i) = w;
    //
    // This relies also on "lookBelowFor".
    //
    def update(i:Int,w:T):Unit = {

	if (0 <= i && i < this.size) {

	    //	    val x:SeqNode[T] = lookBelowFor(this.root,i);
	    //x.value = w;
	    //this.root = x;    // with the splay operation, the updated node becomes the new root.

	    this.root = lookBelowFor(this.root,i);   // second attempt at updating the root/value of i^th node
	    this.root.value = w;
	    
	} else {

	    throw new IndexOutOfRangeException;

	}
    }


    // ******** TRAVERSAL METHODS ********

    //
    // toString
    //
    // This converts a sequence into its string representation.
    // A sequence containing 3,1,8,5 has the string "[3,1,8,5]".
    //
    // Its code relies on the implementation of the foreach
    // method, that follows.
    //
    // Scala use: (example)
    //
    //     println(S);
    //
    // The above example code relies implicitly on the toString
    // method in order to output a string representation of S to
    // the console.
    //
    override def toString:String = {
	var s:String = "[";
	var first:Boolean = true;
	for (v <- this) {
	    if (first) {
		first = false;
	    } else {
		s += ",";
	    }
	    s += v;
	}
	return s + "]";
    }

    //
    // foreach
    //
    // This executes some visit procedure on each value in the Sequence
    // according to their position in the Seequence.  It happens to be
    // the code used by any FOR loop driven by a Sequence S.
    //
    // Scala use:
    //
    //     for (v <- S) {
    //       ...
    //     }
    //
    // In the code above, the curly-braced loop body would constitute the
    // visit procedure. See toString for an example.
    //
    def foreach(visit:(T) => Unit):Unit = {
	traverse(this.root,visit)
    }

    // 
    // traverse
    //
    // This performs an IN-ORDER traversal of the values of the nodes
    // of the subtree of x, representing a slice of a Sequence.
    //
    def traverse(x:SeqNode[T],visit:(T) => Unit):Unit = { 
	if (x != null) {
	    traverse(x.left,visit);
	    visit(x.value);
	    traverse(x.right,visit);
	}
    }

    // ******** HELPER METHODS ********

    //
    //  lookBelowFor
    //
    def lookBelowFor(x:SeqNode[T],i:Int):SeqNode[T] = {
	
	var belowTerm:SeqNode[T] = null; // Added because perhaps I need to use this to store the result of
				         // recursive calls to lookBelowFor before I return {zig/zag}* of them.
	
	if (x == null) {

	    throw new IndexOutOfRangeException;

	} else {
	    // NEED: Manage the cases wherein empty subtrees get in the way of rotations, etc.
	    //    those pesky null-pointers!

	    if (i == x.leftSize) {  // lookBelowFor has bottomed out. Return sought-after node.
		return x;
	    }



	    // If the i^th term comes before x in the sequence, will be a splay-operation beginning with "ZIG"
	    else if (i < x.leftSize) {   

		if (i < (x.left).leftSize) {         //case ZIG-ZIG
		    belowTerm = lookBelowFor( (x.left).left, i )   
		    x.left.left = belowTerm;
		    return zigzigBelow(x);
		}

		else if (i > (x.left).leftSize) {    //case ZIG-ZAG
		    belowTerm = lookBelowFor( (x.left).right, i-(x.left).leftSize-1)   // Again, unsure if this helps anything. But hey, it's 4am.
		    x.left.right = belowTerm;
		    return zigzagBelow(x);
		}
	  
		else {
		    if (i == (x.left).leftSize) {return zigBelow(x);}   //case ZIG  (only one splay-step away, not 2. lokBelowFor bottoms out.)
		    else { throw new IndexOutOfRangeException; }       // Your tree is malformed if you get to this line.
		}
	    }



	    
	    // Otherwise, will be a splay-operation beginning with "ZAG"
	    else { //[ if (i > x.leftSize) ]   
		//i = (i-x.leftSize-1);
		if (i-x.leftSize-1 < (x.right).leftSize ) {          // Is i^th node in (x.right)'s left subtree?
		    belowTerm = lookBelowFor((x.right).left, i)    // Again, unsure if this helps anything. But hey, it's 4am.
		    (x.right).left = belowTerm;
		    return zagzigBelow(x); 
		}
		else if (i-x.leftSize-1 > (x.right).leftSize) {
		    belowTerm =  lookBelowFor((x.right).right, i-(x.right).leftSize-1)      // Again, unsure if this helps anything. But hey, it's 4am.
		    (x.right).right = belowTerm;
		    return zagzagBelow(x);
		}

		else {
		    if (i-x.leftSize-1 == (x.right).leftSize) {return zagBelow(x) }     //case ZAG
		    else { throw new IndexOutOfRangeException; }
		}

		
		// Thoughts: Unsure about whether I'm applying the {zig/zag}* operations to the correct nodes.
		//     
		
	    }

	}

    }

    //
    //  insertBelowAt
    //
    def insertBelowAt(x:SeqNode[T],i:Int,w:T):SeqNode[T] = {

	if (x == null) {

	    return new SeqNode[T](w);

	} else {

	    if (i <= x.leftSize) {
		x.left = insertBelowAt(x.left,i,w);
		x.leftSize = x.leftSize+1;
	    } else {
		x.right = insertBelowAt(x.right,i-x.leftSize-1,w);
	    }

	    return x;

	}

    }

    //
    // rightRotateAt
    //
    def rightRotateAt(x:SeqNode[T]):SeqNode[T] = {
	val w:SeqNode[T] = x.left;
	
	// println("Currently in rightRotateAt, this sequence is:  " + this);

	// refigure links
	x.left = w.right;
	w.right = x;
	
	// adjust size of x's left
	x.leftSize = x.leftSize - w.leftSize - 1;

	// return the new root
	return w;

    }

    //
    // leftRotateAt
    //
    def leftRotateAt(x:SeqNode[T]):SeqNode[T] = {
	val y:SeqNode[T] = x.right;

	// println("Currently in leftRotateAt, this sequence is:  " + this);
	
	// refigure links
	x.right = y.left;
	y.left = x;
	
	// adjust size of x's left
	y.leftSize = y.leftSize + x.leftSize + 1;

	// return the new root
	return y;

    }

    //
    // zig-zig
    //
    def zigzigBelow(g:SeqNode[T]):SeqNode[T] = {
	println("zigzig-ing node ["+g.value+"]")
	

	return rightRotateAt(rightRotateAt(g));
    }

    //
    // zig-zag
    //
    def zigzagBelow(g:SeqNode[T]):SeqNode[T] = {
	g.left = leftRotateAt(g.left);
	println("zigzag-ing")
	return rightRotateAt(g);
    }

    //
    // zag-zag
    //
    def zagzagBelow(g:SeqNode[T]):SeqNode[T] = {
	println("zagzag-ing node ["+g.value+"]")
	return leftRotateAt(leftRotateAt(g));
    }

    //
    // zag-zig
    //
    def zagzigBelow(g:SeqNode[T]):SeqNode[T] = {
	g.right = rightRotateAt(g.right);
	println("zagzig-ing")
	return leftRotateAt(g);
    }

    //
    // zig
    //
    def zigBelow(p:SeqNode[T]):SeqNode[T] = {
	return rightRotateAt(p);
    }

    //
    // zag
    //
    def zagBelow(p:SeqNode[T]):SeqNode[T] = {
	println("zag-ing node ["+p.value+"]")
	return leftRotateAt(p);
    }

}

//
// ASSIGNMENT
//
// The problem with the implementation of Sequence, above, is
// that a series of inserts can lead to a poorly-performing
// tree.  To avoid that possibility, we should modify the
// binary tree implementation using one of our balanced tree
// variants.
//
// 1. Modify the lookBelowFor code so that it uses splay steps (zig,
//    zag, etc.) to SPLAY the accessed node to the root of the 
//    Sequence's tree.
//
// In order to see whether your tree SPLAYing is doing the right 
// thing, I'd like you write the following code:
//
// 2. Add a method called "display" that displays (i.e. outputs
//    text to the console showing) the underlying binary tree
//    structure of the Sequence.
//
// Note that, in order to display the tree structure, you will
// want to (essentially) compute coordinates for the placement
// of nodes's values in the displayed figure.  This will likely
// require you to add fields to each node that contain information
// necessary for the display (necessary for calculating a coordinate)
// You can then write tree traversal code that computes that extra
// information--- (re)calculated with each display request--- and
// use that calculated information during a traversal that produces
// the displayed figure.
//

