//
// MATH 382 
//
// HW6 program: Displaying the Splay 
//
// Graphics Code
// -------------
//
// The code below can be used as the starting point,
// along with the code in Sequence.scala, for your
// inplementation of a graphics displayable Sequence
// data structure.  It defines three object types:
//
//   SequencePanel[T] - a displayed graphics element
//      that provides a visual representation of a
//      Sequence[T] data structure.  
//
//   DisplaySequence[T] - a specialized subclass of
//      Sequence[T], it provides two additional 
//      methods:
//
//      1. prepareToDisplay, which traverses the
//         nodes of a Sequence and determines their
//         row and column in the visual display of
//         the tree structure; and   
//
//      2. display, which renders the tree structure
//         inside a graphics context.
//
//      It also contains methods useful for the above,
//      for drawing nodes and edges of the tree 
//      structure.  It overrides some of the major
//      Sequence methods so that, should the sequence
//      change, the picture of the Sequence is redrawn.
//
//   Graphics - a singleton class that contains a bunch
//      of convenient procedures for drawing graphics
//      in a window.
//
// Should you wish to complete the assignment using this
// code, you should write the code for the two methods
// (1) and (2) of DisplaySequence[T], above.
//
// Right now, they simply have code that illustrate how
// portions of a tree can be rendered.
//
//

import javax.swing._;
import java.awt._;
import java.awt.font._;
import java.awt.geom._;
// import swing.{Panel, MainFrame, SimpleSwingApplication}

//
// class SequencePanel[T]
//
// Implementation of a Java window component (a JPanel) whose
// content is the rendering of the tree structure of a Sequence.
//
class SequencePanel[T](S:DisplaySequence[T]) extends JPanel {

  // initialization code
  val WIDTH: Int = 20;
  val HEIGHT:Int = 20;
  val data:DisplaySequence[T] = S;
  setPreferredSize(new Dimension(Graphics.WINDOW_WIDTH, Graphics.WINDOW_WIDTH));

  // paintComponent
  //
  // This code runs every time the window believes that
  // this panel's content needs to be redrawn.
  //
  override def paintComponent(g: Graphics) {
    g match {
      case g2d:Graphics2D => {
	S.prepareToDisplay;
	Graphics.drawInit(g2d);
	S.display(g2d);
      }
      case _ => {}
    }
  }

}

//
// class DisplaySequence[T]
//
// This is a partial implementation of a Sequence data structure 
// that, when created, opens a Java window displaying a "live"
// view of its underlying tree structure.  See the solution for
// an example of a fully functional version.
//
// It inherits all the methods of Sequence[T], and also provides
// the methods
//
//    prepareToDisplay
//    display
//
// described above and below, as well as drawNode, drawEdge,
// and refresh.  The last of these is called each time the
// Sequence changes its structure, and leads to a refresh of
// the Sequence display.
//
class DisplaySequence[T] extends Sequence[T] {

    // initialization code
    val frame:JFrame = new JFrame("");
    frame.setContentPane(new SequencePanel[T](this));
    frame.pack();
    frame.setResizable(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

    //
    // drawNode
    //
    // Renders a labeled dot on the display (given by the
    // graphics context object g), corresponding to the 
    // SeqNode x.  It relies on x having a row and a column
    // calculated by prepareToDisplay.
    //
    def drawNode(g:Graphics2D,x:SeqNode[T]):Unit = {
	Graphics.drawDot(g,x.value.toString,x.column,x.row);
    }

    //
    // drawNode
    //
    // Renders a line segment on the display (given by the
    // graphics context object g), connecting the SeqNodes
    // x and y.  It relies on x and y each having a row and 
    // a column calculated by prepareToDisplay.
    //
    def drawEdge(g:Graphics2D,x:SeqNode[T],y:SeqNode[T]):Unit = {
	Graphics.drawSegment(g,x.column,x.row,y.column,y.row);
    }

    //
    // refresh
    //
    // Gets called with each change to the Sequence's tree 
    // structure, and initiates a request to the window to
    // redraw the structure.
    //
    def refresh:Unit = {
	frame.repaint();
    }

    //
    // prepareToDisplay
    //
    // * UNFINISHED CODE *
    //
    // This code, ideally, should lead to setting of the
    // row and column fields of each tree node that makes
    // up the Sequence, so that the code for "display" can
    // do its work.
    //
    def prepareToDisplay:Unit = {
	if (root != null) {
	    root.row = 0; 
	    root.column = root.leftSize;   // root's leftsubtree-size is its column coordinate.

	    if (root.left != null) {
		root.left.row = 1;
		root.left.column = 0;
	    }

	    if (root.right != null) {
		root.right.row = 1;
		root.right.column = 2;
	    }
	}
    }

  
    //
    // display
    //
    // * UNFINISHED CODE *
    //
    // This code, ideally, should lead to a drawing of all 
    // the nodes and edges (links) of the tree of this
    // Sequence.
    //
    def display(g:Graphics2D):Unit = {
	if (root != null) {
	    if (root.left != null) {
		drawEdge(g,root,root.left);
		drawNode(g,root.left);
	    }

	    if (root.right != null) {
		drawEdge(g,root,root.right);
		drawNode(g,root.right);
	    }

	    drawNode(g,root);
	}
    }


    //
    // overridden ADT methods
    //
    // These are the major ethods of Sequence, modified
    // so that their use causes a refresh of the display
    // window's rendering of this Sequence.
    //
    override def apply(i:Int):T = {
	val v:T = super.apply(i);
	refresh;
	return v;
    }
    //
    override def update(i:Int,w:T):Unit = {
	super.update(i,w);
	refresh;
    }
    //
    override def +=(i:Int,v:T):Unit = {
	super.+=(i,v);
	refresh;
    }
    //
    override def +=(v:T):Unit = {
	super.+=(v);
	refresh;
    }

}

//
// Graphics
//
// This singleton object contains a collection of useful routines
// and constants for rendering a tree.  The methods all draw, using
// a variety of Java AWT and Swing components and procedures, into
// a given graphics context.
//
object Graphics {

  val WINDOW_WIDTH:Int = 800;
  val WIDTH: Int = 16;
  val HEIGHT:Int = 16;

  val PAPER_COLOR:Color = new Color(0.66f,0.8f,0.5f);
  val GRID_COLOR:Color  = new Color(0.23f,0.45f,0.2f);
  val FILL_COLOR:Color  = new Color(0.5f,0.4f,0.8f)
  val LINE_COLOR:Color  = new Color(0.9f,0.4f,0.45f);
  val DOT_WIDTH:Float  = 0.8f;
  val LINE_WIDTH:Float = 0.1f;

  def drawSegment(g:Graphics2D, x1:Float, y1:Float, x2:Float, y2:Float):Unit = {
    g.setStroke(new BasicStroke(LINE_WIDTH,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
    g.setColor(LINE_COLOR);
    g.draw(new Line2D.Float(x1,y1,x2,y2));
  }

  def drawDot(g:Graphics2D, s:String, x:Float, y:Float):Unit = {

    // figure out where the center of the string is
    g.setFont(new Font("Lucida Grande", Font.BOLD, 48));
    val fontMetrics:FontMetrics = g.getFontMetrics();
    val stringBounds:Rectangle = fontMetrics.getStringBounds(s, g).getBounds();
    val font:Font = g.getFont();
    val renderContext:FontRenderContext = g.getFontRenderContext();
    val glyphVector:GlyphVector = font.createGlyphVector(renderContext, s);
    val visualBounds:Rectangle = glyphVector.getVisualBounds().getBounds();

    // save the coordinate system
    val t:AffineTransform = g.getTransform();

    // draw the filled region
    g.setColor(FILL_COLOR);
    g.translate(x,y);
    g.scale(DOT_WIDTH/2.0f,DOT_WIDTH/2.0f);
    g.fill(new Ellipse2D.Float(-1.0f,-1.0f,2.0f,2.0f));     

    // draw the outline
    g.setColor(LINE_COLOR);
    g.setStroke(new BasicStroke(0.15f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));   
    g.draw(new Ellipse2D.Float(-1.0f,-1.0f,2.0f,2.0f));     

    // draw the label's text
    g.scale(0.8/visualBounds.height,0.8/visualBounds.height);
    val textX:Float = -stringBounds.width/2.0f;
    val textY:Float = -visualBounds.height/2.0f - visualBounds.y;
    g.drawString(s, textX, textY);

    // restore the coordinate system
    g.setTransform(t);

  }

  def drawInit(g:Graphics2D) {
    g.scale(WINDOW_WIDTH/(WIDTH+2),WINDOW_WIDTH/(WIDTH+2));

    // move the origin point
    g.translate(1.0f,1.0f);

    // make lines appear soft
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			 RenderingHints.VALUE_ANTIALIAS_ON);
    
    // draw the grid lines
    drawGrid(g);

  }

  def drawGrid(g:Graphics2D) {

    g.setColor(GRID_COLOR);

    // draw minor grid lines
    g.setStroke(new BasicStroke(0.001f));
    val delta:Float = 0.1f;
    for (i <- 0 to 10*WIDTH) g.draw(new Line2D.Float(i*delta,0.0f,
						       i*delta, HEIGHT));
    for (i <- 0 to 10*HEIGHT) g.draw(new Line2D.Float(0.0f, i*delta,
							WIDTH,i*delta));

    // draw major gridlines
    g.setStroke(new BasicStroke(0.005f));
    for (i <- 0 to WIDTH) g.draw(new Line2D.Float(i*delta*10, 0.0f,
						    i*delta*10, HEIGHT));
    for (i <- 0 to HEIGHT)  g.draw(new Line2D.Float(0.0f, i*delta*10,
						      WIDTH,i*delta*10));
  }

}


