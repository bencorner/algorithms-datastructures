//   Ben Corner
//   MATH382 -- HW #1
//   13 Feb. 2013


object hw01_v2 {

      //
      // bubblesort
      //
      // Sorts using the magical power of bubbles.
      // 

      def bubbleSort(a:Array[Int]):Unit = {
	    val n:Int = a.length;
	    var i:Int = 1;
	    var j:Int = n-1;
	    var temp:Int = 0;		  
	    
	    for (i <- 0 to n-1) {
		  for (j <- n-1 to i+1 by -1) {
			temp = a(j);
			
			if ( a(j) < a(j-1) ) { // Swap adjacent values that need to be swapped.
			      a(j) = a(j-1);
			      a(j-1) = temp;
			}
		  }
	    }     
      }	




      //
      // selection sort
      //
      // Fairly straightforward implementation of selection-sort.
      // 

      def selectionSort(a:Array[Int]):Unit = {
	    val n:Int = a.length;
	    var j:Int = 0;
	    var min:Int = 0;
	    var temp:Int = 0
	    
	    for (j <- 0 to n-1) {
		  
		  min = j;
		  
		  for (i <- j to n-1) {

			if (a(i) < a(min)) {
			      min = i;
			}
		  }
		  
		  if (min != j) {
			temp = a(j);
			a(j) = a(min);
			a(min) = temp;
		  }
	    }
      }
  
      //
      // printArray
      //
      // A simple function to print the contents of an array.
      def printArray(a:Array[Int]):Unit = {

	    for (v <- a) {
		  print(v + " ");
	    }
	    println();
	    println();
      }

      

      //
      // test
      //
      // When passed an array of values, test creates a copy of the array
      // (so one of the sorts doesn't end up being tested with the sorted
      // output of the first sorting algorithm).
      //
      // Then, it runs through and prints out the array contents both before
      // and after application of each of the sorting algorithms.


      def test(a:Array[Int]):Unit = {
	    
	    var b:Array[Int] = new Array[Int](a.length);
	    Array.copy(a, 0, b, 0, a.length)

	    print("RUNNING TEST CASE");
	    println();

	    println("PRE-SORT: Copy a");   // Prints initial value of array 'a'
	    printArray(a);

	    println("Using bubblesort:");  // Applies bubbleSort to a.
	    bubbleSort(a)
	    printArray(a)	




	    println("PRE-SORT: Copy b");   // Prints initial value of array 'b'
	    printArray(b);

	    println("Using selection-sort"); // Applies selection-sort to b.
	    selectionSort(b);
	    printArray(b);

	    println("********  DONE WITH BOTH SORTS  ********");
	    println();
      }





      //
      // main
      //
      // Calls for test() of either
      //       i.)  an array passed through command line args
      //       ii.) a small assortment of various other hard-coded arrays
      //
      def main(args:Array[String]):Unit = {

	    val ints:Array[Int] = new Array[Int](args.length);

	    for (i <- 0 until args.length) {
		  ints(i) = args(i).toInt;
	    }

	    test(ints);   //// Sorting the command-line arg input


	    //////// RUN SOME TESTS HERE FOR FUN IF NO INPUT GIVEN ////////
	    if (ints.length == 0) {
		  test(Array(7,4,5,3,2,9,13,1,3,1));
		  test(Array(1,2,2,2,2,2,2,2,2));
		  test(Array(1,1,1,1,1,1,1,1,2));
		  test(Array());
		  test(Array(0));
		  test(Array(10,1,2,3,4,5,6,7,8,9,10));
	    }
      }
}
  

