//   Ben Corner
//   MATH382 -- HW #1
//   13 Feb. 2013


object hw01 {

      //
      // bubblesort
      //
      // 
      // 

      def bubbleSort(a:Array[Int]):Unit = {
	    val n:Int = a.length;
	    var i:Int = 1;
	    var j:Int = n-1;
	    var temp:Int = 0;		  
	    
	    for (i <- 0 to n-1) {
		  for (j <- n-1 to i+1 by -1) {
			temp = a(j);
			
			if ( a(j) < a(j-1) ) { 
			      a(j) = a(j-1);
			      a(j-1) = temp;
			}
		  }
	    }     
      }	




      //
      // selection sort
      //
      //
      // 

      def selectionSort(a:Array[Int]):Unit = {
	    val n:Int = a.length;
	    var j:Int = 0;
	    var min:Int = 0;
	    var temp:Int = 0
	    
	    for (j <- 0 to n-1) {
		  
		  min = j;
		  
		  for (i <- j to n-1) {

			if (a(i) < a(min)) {
			      min = i;
			}
		  }
		  
		  if (min != j) {
			temp = a(j);
			a(j) = a(min);
			a(min) = temp;
		  }
	    }
      }
  
      //
      // main
      //
      // Tests insertion sort and merge sort.
      //
      def main(args:Array[String]):Unit = {

	    val ints:Array[Int] = new Array[Int](args.length);

	    for (i <- 0 until args.length) {
		  ints(i) = args(i).toInt;
	    }

	    print("BEFORE:")
	    for (v <- ints) {
		  print(v + " ");
	    }
	    println();

	    // Test bubble sort.
	    bubbleSort(ints);
      
	    println("AFTER BSORT:")
	    for (v <- ints) {
		  print(v + " ");
	    }
	    println();

	    // Now test selectionSort.
	    for (i <- 0 until args.length) {
		  ints(i) = args(i).toInt;
	    }

	    selectionSort(ints);
      
	    println("AFTER SSORT:")
	    for (v <- ints) {
		  print(v + " ");
	    }
	    println();

      }
}
  

