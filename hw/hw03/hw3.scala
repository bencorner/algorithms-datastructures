// MATH 382 HOMEWORK 3 EXERCISE 1
//
// Name: Ben Corner
// Login: bcorner
//

object hw3 {

      // select( i ,  array a )
      //
      // Takes an "order" and an array, calls selectHelper to
      // find and return the value of the i-th order element of the array.

      def select(i:Int, a:Array[Int]):Int = {
	    selectHelper(i,a,0,a.length-1);
      }
      
      def selectHelper(i:Int, a:Array[Int], p:Int, r:Int):Int = {	    
	    
	    val q = partition(a,p,r);
	    if (i < q)                             // if the pivot-value comes to rest at an index less than i,
		  return selectHelper(i,a,p,q-1);  // examine the partition that must contain the wanted value.

	    else if (i > q)
		  return selectHelper(i,a,q+1,r);
	    else
		  return a(q);                     // selectHelper returns the i-th order element
				                   // precisely when the pivot-value comes to rest
	                                           // in the i-th slot.
      }



      // partition( array a , left , right )
      //
      // Takes an array together with the range of indices over which
      // we are partitioning the array. This implementation of partition
      // uses the right-most value in the array-range as the pivot-value.

      def partition(a:Array[Int],p:Int,r:Int):Int = {
	    
	    // choose rightmost value as pivot, x.

	    val x:Int = a(r);
	    var i:Int = p-1;
	    var j:Int = 0;
	    var temp:Int = 0;
		  
	    for (j <- p to r-1) { // Iterate over the array segment to be partitioned
		  if (a(j) <= x) {  // If a value is less than the pivot value, then:
			i = i + 1;			// record this by incrementing i
			temp = a(j);			// exchange a(i) with a(j)
			a(j) = a(i); 
			a(i) = temp;
		  }
		  
	    }
	    // exchange a(i+1) with a(r)
	    temp = a(i+1);
	    a(i+1) = a(r);
	    a(r) = temp;
	    
	    return (i+1);
		  
      }



      
      def main(args:Array[String]):Unit = {

	    //
	    // get the order index 
	    val which:Int = args(0).toInt;

	    //
	    // build an array of the values to select from
	    val values:Array[Int] = new Array[Int](args.length-1);
	    print("Selecting element "+which+" from amongst the values ")
	    for (i <- 1 until args.length) {
		  print(args(i)+(if (i<args.length-1) {","} else {""}));
		  values(i-1) = args(i).toInt;
	    }
	    println();

	    println("==> "+ select(which,values));  // actually runs the SELECT function
	    
      }
}

      
