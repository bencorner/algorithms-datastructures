
object lec1_2 {

  //
  // insertionSort
  // 
  // Sorts an array of integers using insertion sort.
  //
  // This code is nearly the same as CLRS 2.1 page 18,
  // except the index set of a scala array of length n 
  // is {0,...,n-1} rather than {1,...,n}.  Also, in
  // the inner loop I've made position i the target of 
  // the insertion (rather than i+1).
  //
  def insertionSort(a:Array[Int]):Unit = {
    val n:Int = a.length;

    // for each position i
    for (j <- 1 to n-1) {

      // insert a[j] into a[0..j] by finding its place i
      val key:Int = a(j);
      var i:Int = j;

      // scan left past values greater than a[i]
      while (i > 0 && a(i-1) > key) {
	// shifting each such value one place right
	a(i) = a(i-1);
	i = i-1;
      }
      // place a[j] at position i
      a(i) = key;
    }
  }    

  //
  // mergeSort
  //
  // Sorts an array of integers using mergesort.
  //
  // This code is the top-level procedure that
  // invokes the recursive procedure for mergeSort.
  //
  def mergeSort(a:Array[Int]):Unit = {
    val n:Int = a.length;
    mergeSortRec(a,0,n-1);
  }
  
  // 
  // mergeSortRec
  //
  // This recursively sorts the array a within
  // the index range p..r.  It is exactly the 
  // algorithm described in CLRS 2.3 and this 
  // code is given as pseudo-code on page 34.
  //
  // It relies on the procedure merge, below,
  // to merge two recursively sorted sub-ranges.
  //
  def mergeSortRec(a:Array[Int],p:Int,r:Int):Unit = {
    
    // if the array range to sort is longer than 1
    if (p < r) {
      
      // compute the split index
      val q:Int = (p + r)/2;   // note that this is integer division
      // nor fractional part
      
      mergeSortRec(a, p, q);
      mergeSortRec(a, q+1, r);
      merge(a, p, q, r);
    }
  }
  
  //
  // merge
  //
  // This procedure merges two sorted portions of an array, the
  // elements in range p..q, and the elements in range q+1..r.  The
  // resulting sorted values are put in the range p..r.  
  //
  // It copies the two portions into arrays left and right in order
  // to merge those values back into the combined portion in a.
  // 
  def merge(a:Array[Int], p:Int, q:Int, r:Int) {
    
    // make a copy of the left half, with an
    // aditional "sentinel" value of infinity.
    // (see CLRS page 31).
    //
    val ln:Int = q - p + 1;
    val left:Array[Int] = new Array[Int](ln+1);
    
    for (i <- 0 until ln) {
      left(i) = a(p+i);
    }
    left(ln) = Int.MaxValue;
    
    // make a copy of the right half
    val rn:Int = r - q;
    val right:Array[Int] = new Array[Int](rn+1);
    
    for (i <- 0 until rn) {
      right(i) = a(q+1+i);
    }
    right(rn) = Int.MaxValue;
    
    var li:Int = 0;
    var ri:Int = 0;
    for (i <- p to r) {
      if (left(li) <= right(ri)) {
	a(i) = left(li);
	li = li + 1;
      } else {
	a(i) = right(ri);
	ri = ri + 1;
      }
    }
  }
  
  //
  // main
  //
    // Tests insertion sort and merge sort.
    //
    def main(args:Array[String]):Unit = {

      val ints:Array[Int] = new Array[Int](args.length);

      for (i <- 0 until args.length) {
	ints(i) = args(i).toInt;
      }

      print("BEFORE:")
      for (v <- ints) {
	print(v + " ");
      }
      println();

      // Test insertion sort.
      insertionSort(ints);
      
      println("AFTER ISORT:")
      for (v <- ints) {
	print(v + " ");
      }
      println();

      // Now test mergesort.
      for (i <- 0 until args.length) {
	ints(i) = args(i).toInt;
      }

      mergeSort(ints);
      
      println("AFTER MSORT:")
      for (v <- ints) {
	print(v + " ");
      }
      println();

    }
  }
  

